package form;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import message.Message;
import message.MessageAlert;
import objects.Circle;
import objects.Figure;
import objects.Square;

public class Controller {

    @FXML
    public Spinner size;
    public ColorPicker colorPicker;
    public ToggleButton circle, square;
    public Canvas canvas;
    public MenuItem circleItem, squareItem;
    public Menu menu;
    public Pane pane;
    private String selectedIndex;
    private Message message = new MessageAlert();


    public void loadSpinner(){
        SpinnerValueFactory<Integer> valueFactory = new SpinnerValueFactory
                .IntegerSpinnerValueFactory(1, 100, 15);
        size.setValueFactory(valueFactory);
        menu.setOnAction(e -> { selectedIndex = ((MenuItem)e.getTarget()).getText(); });
    }

    public void paintFigure(MouseEvent mouseEvent){
        if (circle.isSelected()) {
            Figure newCircle = new Circle(mouseEvent.getX(),mouseEvent.getY(),
                    Double.valueOf(size.getValue().toString()),colorPicker.getValue());
            newCircle.draw(canvas);
        } else if (square.isSelected()) {
            Figure newSquare = new Square(mouseEvent.getX(), mouseEvent.getY(),
                    Double.valueOf(size.getValue().toString()), colorPicker.getValue());
            newSquare.draw(canvas);
        }
    }

    public void selectFigure() {
        switch (selectedIndex) {
            case "����":  circle.setSelected(true); break;
            case "�������": square.setSelected(true); break;
            default: message.showError("����������� ����������"); break;
        }

    }

    public void exit(){
        if (message.showConfirmation("�� ������������� ������ �����?").getResult() == ButtonType.YES)
            System.exit(0);
    }

    public void clear(){
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.clearRect(0,0,canvas.getWidth(), canvas.getHeight());
    }

}
