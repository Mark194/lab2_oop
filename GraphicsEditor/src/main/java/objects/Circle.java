package objects;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure {

    public Circle(double x, double y, double size, Color color){
        super(x, y, size, color);
    }

    @Override
    public void draw(Canvas canvas) {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setStroke(this.getColor());
        graphicsContext.strokeOval(this.getX(), this.getY(), this.getSize(), this.getSize());
    }



}
